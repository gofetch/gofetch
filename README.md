# Gofetch

Small *fetch program built with Golang.

Displays distro, host, CPU name, package count, DE/WM and uptime.

![image](images/gofetch.png)
