module gitlab.com/gkeep_/gofetch

go 1.16

require (
	github.com/klauspost/cpuid/v2 v2.0.11
	gopkg.in/yaml.v2 v2.4.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
